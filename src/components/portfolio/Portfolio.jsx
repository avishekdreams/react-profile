import React, { useEffect, useState } from 'react';
import PortfolioList from '../portfolioList/PortfolioList';
import './portfolio.scss';
import { employee_relations, payroll_management, general_operations, employee_benefit_management, handle_disciplinary_actions } from '../../data';

export default function Portfolio() {
    const [selected, setSelected] = useState('employee_relations');
    const [data, setData] = useState([]);
    const list = [
        { id: "employee_relations", title: "Employee Relations" },
        { id: "payroll_management", title: "Payroll Management" },
        { id: "general_operations", title: "General Operations" },
        { id: "employee_benefit_management", title: "Employee Benefit Management" },
        { id: "handle_disciplinary_actions", title: "Handle Disciplinary Actions" },
    ]

    useEffect(() => {
        switch (selected) {
            case 'payroll_management': setData(payroll_management); break;
            case 'general_operations': setData(general_operations); break;
            case 'employee_benefit_management': setData(employee_benefit_management); break;
            case 'handle_disciplinary_actions': setData(handle_disciplinary_actions); break;
            default: setData(employee_relations); break
        }
    }, [selected]);

    return (
        <div className='portfolio' id='portfolio'>
            <h1>Portfolio</h1>
            <ul>
                {list.map((item) => (
                    <PortfolioList
                        title={item.title}
                        key={item.id}
                        id={item.id}
                        active={selected === item.id}
                        setSelected={setSelected}
                    />
                ))}
            </ul>
            <div className="container">
                {data.map((val) => (
                    <div className="item">
                        <img src={val.img} alt="" />
                        <h3>{val.title}</h3>
                    </div>
                ))}

            </div>
        </div>
    )
}