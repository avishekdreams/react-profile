import React from 'react';
import './menu.scss';

export default function Lists({data, setMenuOpen}) {
    return (
        <>
            <li onClick={() => setMenuOpen(false)}>
                <a href={`${data.key}`}>{data.name}</a>
            </li>
        </>
    )
}