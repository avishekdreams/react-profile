import React from 'react';
import Lists from './Lists';
import './menu.scss';

const menu = [
    { 'key': '#intro', 'name': 'Home' },
    { 'key': '#portfolio', 'name': 'Portfolio' },
    { 'key': '#works', 'name': 'Works' },
    { 'key': '#testimonials', 'name': 'Testimonials' },
    { 'key': '#contact', 'name': 'Contact' }
]

export default function Menu({ menuOpen, setMenuOpen }) {
    return (
        <div className={"menu " + (menuOpen && "active")}>
            <ul>
                {menu.map((val) => (
                    <Lists data={val} setMenuOpen={setMenuOpen} key={val.key} />
                ))}
            </ul>
        </div>
    )
}