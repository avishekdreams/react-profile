import React, { useEffect, useRef } from 'react';
import './intro.scss';
import Typewriter from 'typewriter-effect';

export default function Intro() {

    return (
        <div className='intro' id='intro'>
            <div className="left">
                <div className="imgContainer">
                    <img src="assets/char2.png" alt="" />
                </div>
            </div>
            <div className="right">
                <div className="wrapper">
                    <h2>Hi there, I'm Suranjika</h2>
                    <h1>Human Resource</h1>
                    <h3>Associate Manager & &nbsp;
                        <Typewriter
                            options={{
                                autoStart: true,
                                loop: true,
                                delay: 75,
                                strings: [
                                    "Recruitment and Hiring",
                                    "Training and Development",
                                    "Employer-Employee Relations",
                                    "Maintain Company Culture",
                                    "Manage Employee Benefits",
                                    "Create a Safe Work Environment",
                                    "Handle Disciplinary Actions"
                                ],
                            }}
                        />
                    </h3>
                </div>
                <a href="#portfolio">
                    <img src="assets/down.png" alt="" />
                </a>
            </div>
        </div>
    )
}